import React, { Component } from "react";
import Login from "./components/login";
import Escolas from './components/escola/escolas';
import Escola from './components/escola/escola';
import Professores from './components/professor/professores';
import Professor from './components/professor/professor';
import Dashboard from './components/dashboard';
import Alunos from './components/aluno/alunos';
import Aluno from './components/aluno/aluno';
import Usuarios from './components/usuario/usuarios';
import Usuario from './components/usuario/usuario';
import Profile from './components/profile';
import Notfound from './components/notfound';
import Aside from './components/aside';
import Cenarios from './components/cenario/cenarios';
import Cenario from './components/cenario/cenario';
import Mundos from './components/mundo/mundos';
import Mundo from './components/mundo/mundo';
import Jogos from './components/jogo/jogos';
import Jogo from './components/jogo/jogo';
import Dicionarios from './components/dicionario/dicionarios';
import Dicionario from './components/dicionario/dicionario';

import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
//import { Navigate  } from 'react-router'

const routdata = [
  {'path': '/', 'data': <Dashboard/>},
  {'path': '/escolas', 'data': <Escolas/>},
  {'path': '/escola/:id', 'data': <Escola/>},
  {'path': '/professores', 'data': <Professores/>},
  {'path': '/professor/:id', 'data': <Professor/>},
  {'path': '/alunos', 'data': <Alunos/>},
  {'path': '/aluno/:id', 'data': <Aluno/>},
  {'path': '/usuarios', 'data': <Usuarios/>},
  {'path': '/usuario/:id', 'data': <Usuario/>},
  {'path': '/cenarios', 'data': <Cenarios/>},
  {'path': '/cenario/:id', 'data': <Cenario/>},
  {'path': '/mundos', 'data': <Mundos/>},
  {'path': '/mundo/:id', 'data': <Mundo/>},
  {'path': '/jogos', 'data': <Jogos/>},
  {'path': '/jogo/:id', 'data': <Jogo/>},
  {'path': '/dicionarios', 'data': <Dicionarios/>},
  {'path': '/dicionario/:id', 'data': <Dicionario/>},
]

class App extends Component {

  /*constructor(props) {
    super(props);
  }*/



  render() {

    return (
      <Router>
        <Routes>
          <Route path="/login" element={
            <div className="main__wrap">
              <main className="login-container">
                <div className="card__box">
                  <Login/>
                </div>
              </main>
            </div>
          } />
          {routdata.map((e) => (
            <Route key={e.path} path={e.path} element={
              <div className="main__wrap">
                <main className="container">
                  <aside>
                    <Aside/>
                  </aside>
                  <section>
                    {e.data}
                  </section>
                </main>
              </div>} />
          ))}


            /*<Route path="/" element={
              <div className="main__wrap">
                <main className="container">
                  <aside>
                    <Aside/>
                  </aside>
                  <section>
                    <Dashboard/>
                  </section>
                </main>
              </div>} />
            <Route path="/escolas" element={
              <div className="main__wrap">
                <main className="container">
                  <aside>
                    <Aside/>
                  </aside>
                  <section>
                    <Escolas/>
                  </section>
                </main>
              </div>} />
            <Route path="/escola/:id" element={
              <div className="main__wrap">
                <main className="container">
                  <aside>
                    <Aside/>
                  </aside>
                  <section>
                    <Escola/>
                  </section>
                </main>
              </div>} />
            <Route path="/professores" element={
              <div className="main__wrap">
                <main className="container">
                  <aside>
                    <Aside/>
                  </aside>
                  <section>
                    <Professores/>
                  </section>
                </main>
              </div>} />
            <Route path="/alunos" element={
              <div className="main__wrap">
                <main className="container">
                  <aside>
                    <Aside/>
                  </aside>
                  <section>
                    <Alunos/>
                  </section>
                </main>
              </div>} />*/
            <Route path="/profile" element={
              <div className="main__wrap">
                <main className="container">
                  <aside>
                    <Aside/>
                  </aside>
                  <section>
                    <Profile/>
                  </section>
                </main>
              </div>} />
            /*<Route path="/cenarios" element={
              <div className="main__wrap">
                <main className="container">
                  <aside>
                    <Aside/>
                  </aside>
                  <section>
                    <Cenarios/>
                  </section>
                </main>
              </div>} />
            <Route path="/mundos" element={
              <div className="main__wrap">
                <main className="container">
                  <aside>
                    <Aside/>
                  </aside>
                  <section>
                    <Mundos/>
                  </section>
                </main>
              </div>} />
            <Route path="/jogos" element={
              <div className="main__wrap">
                <main className="container">
                  <aside>
                    <Aside/>
                  </aside>
                  <section>
                    <Jogos/>
                  </section>
                </main>
              </div>} />
            <Route path="/dicionarios" element={
              <div className="main__wrap">
                <main className="container">
                  <aside>
                    <Aside/>
                  </aside>
                  <section>
                    <Dicionarios/>
                  </section>
                </main>
              </div>} />*/
            <Route path="*" element={
              <div className="main__wrap">
                <main className="container">
                  <aside>
                    <Aside/>
                  </aside>
                  <section>
                    <Notfound/>
                  </section>
                </main>
              </div>} />

        </Routes>
      </Router>
    )
  }
}

export default App;
