import React, { Component } from "react";
import HttpMiddleware from "../HttpMiddleware";
import { Navigate  } from 'react-router'

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {login: '', password: '', redirect: null, hover: false};

    this.handleChange = this.handleChange.bind(this);
    this.handleChangePass = this.handleChangePass.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);

    sessionStorage.removeItem('user');
  }

  handleChange(event) {
    this.setState({login: event.target.value});
  }

  handleChangePass(event) {
    this.setState({password: event.target.value});
  }

  setHover( i ){
    this.setState({'hover': i})
  }

  async handleSubmit(e) {

    e.preventDefault();

    var dadosEnviar = {
      'email': this.state.login,
      'password': this.state.password
    }

    var dataRet = await new HttpMiddleware().exec( 'auth/login', 'POST', false, dadosEnviar )

    if ( ! dataRet.data ) {
      alert('Login e/ou senha inválido!')
      return;
    }
    var data1 = await new HttpMiddleware().exec( 'me', 'GET', false, null, 'Bearer ' + dataRet.data.token )

    var user = {
      'token': 'Bearer ' + dataRet.data.token,
      'email': data1.login,
      'nome': data1.nome,
      'tipo': data1.tipo,
      'id': data1.id
    }

    sessionStorage.setItem('user', JSON.stringify(user));

    this.setState({ redirect: "/" });

  }

  render() {
    if (this.state.redirect === '/') {
      return <Navigate to="/"/>
    }

    return (
      <form onSubmit={this.handleSubmit}>
        <img src={process.env.PUBLIC_URL + '/logo.jpg'} style={style.img} alt="Logo"/>
        <label style={style.label}>Login:
          <input  style={style.input} type="email" value={this.state.login} onChange={this.handleChange} />
        </label>
        <label style={style.label}>Senha:
          <input  style={style.input} type="password" value={this.state.password} onChange={this.handleChangePass} />
        </label>
        <input
          style={this.submit(this.state.hover)}
          onPointerOver={()=> this.setHover(true)}
          onPointerOut={() => this.setHover(false)}
          type="submit"
          value="Login" />
      </form>
    );
  }

  submit(hover) {
    return {
      background: '#069633',
      color: '#033135',
      borderRadius: 2,
      borderWidth: 1,
      borderColor: '#fff',
      boxShadow: hover ? "0px 0px 1px #033135" : "none"
    }
  }
}

const style = {
  label: {
    display: 'block',
    fontSize: 12
  },
  input: {
    display: 'block',
    fontSize: 15,
    width: '100%'
  },
  img: {
    width: '100%',
    marginBottom: 10
  }
};

export default Login;
