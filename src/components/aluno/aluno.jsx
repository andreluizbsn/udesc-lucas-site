import React, { useState, useEffect, Fragment } from 'react';
import { useParams } from 'react-router-dom';
import HttpMiddleware from "../../HttpMiddleware";
import Message from '../message';
import { FaPlus, FaMinus } from 'react-icons/fa';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';

const Professor = () => {

  const [idcad, setIdcad] = useState(0);
  const [nome, setNome] = useState('');
  const [email, setEmail] = useState('');
  const [serie, setSerie] = useState('');
  const [escolas, setEscolas] = useState([]);
  const [escolaid, setEscolaid] = useState([]);
  const [escoladata, setEscoladata] = useState([]);
  const [loading, setLoading] = useState(true);
  const [saved, setSaved] = useState(false);
  const [saveMsg, setSaveMsg] = useState('');
  const [saveType, setSaveType] = useState('success');

  let { id } = useParams();
  //const [posts, setPosts] = useState({ post: null, countSecrets: 0, ui: '' });

  useEffect(() => {
    /*if ( id !== 0 ) {
      var url = 'admin/escolas/' + id
      var res = new HttpMiddleware().execPromisse( url, 'GET', true, null );
      res.then((response) => {
        console.log(response.data);
        setData(response.data)
      })
    }*/

    loadingEscolas();

    if ( id !== '0' ) {

      setIdcad(parseInt(id));


      const fetchData = async () =>{

        try {
          var url = 'admin/alunos/' + id
          var res = await new HttpMiddleware().exec( url, 'GET', true, null );
          setNome(res.nome)
          setEmail(res.email)
          setSerie(res.serie)
          setEscolaid(res.escola_id)
          setEscoladata(res.escola)

          console.log('carr');
        } catch (error) {
          console.error(error.message);
        }
      }
      if ( loading ) {
        fetchData();
        setLoading(false);
      }
    }
  }, []);


  const loadingEscolas = () => {
    const lEsc = async () =>{
      try {
        var urlEsc = 'admin/escolas?no_paginate=true';
        var resEsc = await new HttpMiddleware().exec( urlEsc, 'GET', true, null );

        setEscolas(resEsc)

      } catch (error) {
        console.error(error.message);
      }
    }
    lEsc();
  }

  const save = () => {
    setSaved(false)
    setLoading(true)
    var dadosEnviar = {
      'nome': nome,
      'email': email,
      'serie': serie,
      'escola_id': escolaid
    }

    console.log(dadosEnviar);

    const sendData = async () =>{
      try {
        var method = 'POST';
        var url = 'admin/alunos';
        setSaveMsg('Dados salvos com sucesso!')
        setSaveType('success')
        if ( idcad && idcad !== 0 ) {
            method = 'PUT';
            url += '/' + idcad;
            setSaveMsg('Dados alterados com sucesso!')
        }

        var res = await new HttpMiddleware().exec( url, method, true, dadosEnviar );
        console.log(res);
        if ( res.errors ) {
          setSaveMsg(res.errors[0].message)
          setSaveType('fail')
        } else {
          setIdcad( res.id );
        }
        setLoading(false)
        setSaved(true)
      } catch (error) {
        console.error(error.message);
        setLoading(false)
        setSaved(true)
        setSaveMsg('Erro ao salvar dados!')
        setSaveType('fail')
      }
    }

    sendData();
  }

  const getEscola = (event,val) => {
      escolas.forEach(i => {
        if ( i.nome === val ) {
          setEscolaid(i.id)
          setEscoladata(i)
        }
      });

      console.log(escolaid);

  }


  return (
    <div>
      { loading &&
        <div id="loading">
          <span>Carregando...</span>
        </div>
      }
      { saved &&
        <Message duration="5000" message={saveMsg} type={saveType} />
      }

      <h4>Cadastro de Aluno</h4>
      <button style={style.btnnovo} onClick={() => save()}>Salvar</button>
      <hr/>
      <form className='row'>
        <label className="col-sm-12" style={style.label}>Nome:
          <input style={style.input} type="text" value={nome || ''} onChange={e=> setNome(e.target.value)}/>
        </label>
        <label className="col-sm-4" style={style.label}>E-Mail:
          <input style={style.input} type="text" value={email || ''} onChange={e=> setEmail(e.target.value)}/>
        </label>
        <div className="col-sm-4">
          <label style={style.label}>Série:</label>
          <select style={style.input} value={serie || '1'}onChange={e=> setSerie(e.target.value)}>
            <option value="1">Fundamental I</option>
            <option value="2">Fundamental II</option>
          </select>
        </div>
        <div className="col-sm-4">
          <label style={style.label}>Escola:</label>
          <Autocomplete
              value={escolaid}
              options={escolas}
              renderOption={esc => <Fragment>{esc.nome}</Fragment>}
              getOptionLabel={esc => typeof esc.nome === 'string'
                  || esc.nome instanceof String ? esc.nome : ""}
              style={style.inputselect}
              renderInput={params => {
                  return (
                      <TextField
                          {...params}
                          label={escoladata.nome}
                          variant="outlined"
                          fullWidth
                      />
                  )
              }}
              onInputChange={getEscola}
          />
        </div>
      </form>
    </div>
  );
}

const style = {
  label: {
    display: 'block',
    fontSize: 12
  },
  input: {
    display: 'block',
    fontSize: 15,
    padding: 3,
    width: '100%'
  },
  inputselect: {
    display: 'block',
    fontSize: 15,
    padding: 3,
    width: '100%',
    padding: '0px',
    margin: '0px'
  },
  submit: {
    border: '1px solid',
    position: 'absolute',
    right: '4px',
    marginTop: '-40px',
    padding: '3px 10px',
    borderRadius: '0 3px 3px 0'
  },
  editbutton: {
    border: 'none',
    background: 'none'
  },
  btnnovo: {
    padding: '2px 6px 6px',
    border: '1px solid',
    margin: '5px',
    borderRadius: '3px',
    background: '#007bff',
    color: '#fff',
    position: 'absolute',
    right: 0,
    top: 0,
  },
  btnnovoescola: {
    padding: '2px 6px 5px',
    border: '1px solid',
    margin: '5px',
    borderRadius: '3px',
    background: '#007bff',
    color: '#fff',
    fontSize: 15
  },
  btndeleteschool: {
    padding: '2px 6px 5px',
    border: '1px solid',
    margin: '5px',
    borderRadius: '3px',
    background: 'rgb(229 68 68)',
    color: '#fff',
    fontSize: 15
  },
  btnaddschool: {
    padding: '2px 6px 5px',
    border: '1px solid',
    margin: '5px',
    borderRadius: '3px',
    background: 'rgb(8 161 62)',
    color: '#fff',
    fontSize: 15
  },
  schoollist: {
    border: '1px solid #009533',
    marginBottom: '3px',
    borderRadius: '3px',
    background: 'rgb(160 255 193)'
  },
  textescola: {
    fontSize: 20,
    fontWeight: 'bold'
  },
  modal: {
    position: 'fixed',
    right: 0,
    left: 0,
    top: 0,
    bottom: 0,
    background: 'rgba(0,0,0,.5)',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  btnclosemodal: {
    padding: '2px 6px 5px',
    border: '1px solid',
    margin: '5px',
    borderRadius: '3px',
    background: 'rgb(229 68 68)',
    color: '#fff',
    fontSize: 15,
    position: 'absolute',
    right: '5%',
    top: '5%'
  },
  internalmodal: {
    background: '#fff',
    width: '90%',
    height: '90%',
    padding: '10px'
  }
};

export default Professor;
