import React, { useState, useEffect, Fragment } from 'react';
import { useParams } from 'react-router-dom';
import HttpMiddleware from "../../HttpMiddleware";
import Message from '../message';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { FaBook } from 'react-icons/fa';
import TextDict from '../utils/TextDict';

const Mundo = () => {

  const [idcad, setIdcad] = useState(0);
  const [nome, setNome] = useState('');
  const [urlimagebase, setUrlimagebase] = useState('');
  const [ativo, setAtivo] = useState('true');
  const [posicao, setPosicao] = useState(0);
  const [descricao, setDescricao] = useState('');
  const [descricaoFormated, setDescricaoFormated] = useState('');

  const [imageBase64, setImageBase64] = useState('')
  const [imageBase64Final, setImageBase64Final] = useState('')
  const [thumbBase64, setThumbBase64] = useState('')
  const [thumbBase64Final, setThumbBase64Final] = useState('')

  const [cenarios, setCenarios] = useState([]);
  const [cenarioid, setCenarioid] = useState(0);
  const [cenariodata, setCenariodata] = useState({});

  const [dicionarios, setDicionarios] = useState([]);

  const [modal, setModal] = useState(false);
  const [dicionarioChave, setDicionarioChave] = useState('');
  const [dicionarioValor, setDicionarioValor] = useState('');
  const [dictPositions, setDictPositions] = useState([])

  const [loading, setLoading] = useState(true);
  const [saved, setSaved] = useState(false);
  const [saveMsg, setSaveMsg] = useState('');
  const [saveType, setSaveType] = useState('success');

  let { id } = useParams();
  //const [posts, setPosts] = useState({ post: null, countSecrets: 0, ui: '' });

  useEffect(() => {

    loadingCenarios();
    loadingDicionarios();

    if ( id !== '0' ) {

      setIdcad(parseInt(id));

      const fetchData = async () =>{
        try {
          var url = 'admin/mundos/' + id
          var res = await new HttpMiddleware().exec( url, 'GET', true, null );
          setNome(res.nome)
          setUrlimagebase(res.url_image_base)
          setAtivo(res.ativo ? 'true' : 'false')
          setCenarioid(res.cenario_id)
          setCenariodata(res.cenario)
          setPosicao(res.posicao)
          setDescricao(res.descricao)

          setImageBase64(res.image_base_64)
          setImageBase64Final(res.image_base_64_final)
          setThumbBase64(res.thumb_base_64)
          setThumbBase64Final(res.thumb_base_64_final)

          convertDescricao( res.descricao );

        } catch (error) {
          console.error(error.message);
        }
      }
      if ( loading ) {
        fetchData();
        setLoading(false);
      }
    }
  }, []);

  const loadingCenarios = () => {
    const lEsc = async () =>{
      try {
        var urlEsc = 'admin/cenarios?no_paginate=true';
        var resEsc = await new HttpMiddleware().exec( urlEsc, 'GET', true, null );

        setCenarios(resEsc)

      } catch (error) {
        console.error(error.message);
      }
    }
    lEsc();
  }

  const loadingDicionarios = () => {
    const lEsc = async () =>{
      try {
        var urlEsc = 'admin/dicionarios?no_paginate=true';
        var resEsc = await new HttpMiddleware().exec( urlEsc, 'GET', true, null );

        setDicionarios(resEsc)

        console.log(resEsc);

      } catch (error) {
        console.error(error.message);
      }
    }
    lEsc();
  }

  const getCenario = (event,val) => {
      cenarios.forEach(i => {
        if ( i.nome === val ) {
          setCenarioid(i.id)
          setCenariodata(i)
        }
      });

  }

  const save = () => {
    setSaved(false)
    setLoading(true)
    var dadosEnviar = {
      'nome': nome,
      'cenario_id': cenarioid,
      'url_image_base': urlimagebase,
      'image_base_64': '',
      'image_base_64_final': '',
      'thumb_base_64': '',
      'thumb_base_64_final': '',
      'descricao': descricao,
      'ativo': ativo === 'true' ? true : false,
      'posicao': posicao
    }

    console.log(dadosEnviar);

    //return;

    const sendData = async () =>{
      try {
        var method = 'POST';
        var url = 'admin/mundos';
        setSaveMsg('Dados salvos com sucesso!')
        setSaveType('success')
        if ( idcad && idcad !== '0' ) {
            method = 'PUT';
            url += '/' + idcad;
            setSaveMsg('Dados alterados com sucesso!')
        }

        var res = await new HttpMiddleware().exec( url, method, true, dadosEnviar );
        console.log(res);
        if ( res.errors ) {
          setSaveMsg(res.errors[0].message)
          setSaveType('fail')
        } else {
          setIdcad( res.id );
        }
        saveImg(idcad, 0);
        saveImg(idcad, 1);
        saveImg(idcad, 2);
        saveImg(idcad, 3);
        setLoading(false)
        setSaved(true)
      } catch (error) {
        console.error(error);
        setLoading(false)
        setSaved(true)
        setSaveMsg('Erro ao salvar dados!')
        setSaveType('fail')
      }
    }

    sendData();
  }

  const saveImg = (id, ix) => {
    setSaved(false)
    setLoading(true)
    var dadosEnviar;
    var field;
    /*if ( ix === 0 ) {
      dadosEnviar = {
        'image_base_64': imageBase64
      }
    } else if ( ix === 1 ) {
        dadosEnviar = {
          'image_base_64_final': imageBase64Final
        }
    } else if ( ix === 2 ) {
        dadosEnviar = {
          'thumb_base_64': thumbBase64
        }
    } else if ( ix === 3 ) {
      dadosEnviar = {
        'thumb_base_64_final': thumbBase64Final
      }
    }*/

    if ( ix === 0 ) {
      dadosEnviar = imageBase64;
      field = 'image_base_64';
    } else if ( ix === 1 ) {
        dadosEnviar = imageBase64Final
        field = 'image_base_64_final';
    } else if ( ix === 2 ) {
        dadosEnviar = thumbBase64
        field = 'thumb_base_64';
    } else if ( ix === 3 ) {
      dadosEnviar = thumbBase64Final
      field = 'thumb_base_64_final';
    }

    console.log(dadosEnviar);
    console.log(field);

    //return;

    /*const sendData = async () =>{
      try {
        var method = 'PUT';
        var url = 'admin/mundosimg/' + id;
        setSaveMsg('Dados salvos com sucesso!')
        setSaveType('success')

        var res = await new HttpMiddleware().exec( url, method, true, dadosEnviar );
        console.log(res);
        if ( res.errors ) {
          setSaveMsg(res.errors[0].message)
          setSaveType('fail')
        }
        setLoading(false)
        setSaved(true)
      } catch (error) {
        console.error(error.message);
        setLoading(false)
        setSaved(true)
        setSaveMsg('Erro ao salvar dados!')
        setSaveType('fail')
      }
    }*/


    const sendData = async () =>{
      try {
        setSaveMsg('Dados salvos com sucesso!')
        setSaveType('success')

        await new HttpMiddleware().execUpload2( 'mundos', id, field, dadosEnviar );
        setLoading(false)
        setSaved(true)
      } catch (error) {
        console.error(error?.message);
        setLoading(false)
        setSaved(true)
        setSaveMsg('Erro ao salvar dados!')
        setSaveType('fail')
      }
    }

    sendData();
  }

  const getBase64Image = (e, base) => {
    var file = e.target.files[0];

    console.log(file);
    console.log(base);

    var fileSizeMB = file.size / 1024;


    if ( base === 'base' || base === 'basefinal' ) {
      fileSizeMB = fileSizeMB / 1024;
      if ( fileSizeMB > 1 ) {
        setSaved(false)
        setSaveMsg('A imagem possui um tamanho maior que 1 MB!')
        setSaveType('fail')
        setSaved(true)
        return;
      }
    } else {
      if ( fileSizeMB > 100 ) {
        setSaved(false)
        setSaveMsg('A imagem possui um tamanho maior que 100 KB!')
        setSaveType('fail')
        setSaved(true)
        return;
      }
    }

    if ( base === 'base' ) {
      setImageBase64(file)
    } else if ( base === 'basefinal' ) {
      setImageBase64Final(file)
    } else if ( base === 'thumb' ) {
      setThumbBase64(file)
    } else if ( base === 'thumbfinal' ) {
      setThumbBase64Final(file)
    }

    /*let reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = function () {
        console.log(reader.result);
        setUrlimagebase(file.name)
        if ( base === 'base' ) {
          setImageBase64(reader.result)
        } else if ( base === 'basefinal' ) {
          setImageBase64Final(reader.result)
        } else if ( base === 'thumb' ) {
          setThumbBase64(reader.result)
        } else if ( base === 'thumbfinal' ) {
          setThumbBase64Final(reader.result)
        }
    };
    reader.onerror = function (error) {
        console.log('Error: ', error);
    };*/

  }

  const getPosicao = (pos) => {
    console.log(pos);
  }

  const addDicionario = () => {
    var ta = document.querySelector("textarea");
    var dt = ta.value.substring(ta.selectionStart, ta.selectionEnd);

    setDictPositions([ta.selectionStart, ta.selectionEnd]);

    dicionarios.forEach(i => {
      if ( dt.toLowerCase() === i.chave ) {
        setDicionarioValor(i.valor)
      }
    });

    setDicionarioChave( dt.toLowerCase() )
    setModal(true)
  }

  const salvarTextoComDicionario = ( idDict ) => {

    var desc = descricao;

    console.log(idDict);

    desc = desc.substring( 0, dictPositions[0] ) + '{' + idDict + '||' + desc.substring( dictPositions[0], dictPositions[1] ) + '}' + desc.substring( dictPositions[1], desc.length );

    setDescricao(desc)

  }

  const enviarAoDicionario = () => {

    var hasReg = false;

    dicionarios.forEach(i => {
      if ( descricao.substring(dictPositions[0], dictPositions[1]).toLowerCase() === i.chave ) {
        hasReg = true;
        salvarTextoComDicionario(i.id)
      }
    });

    if ( hasReg ) {
      setModal(false);
      setDicionarioChave('')
      setDicionarioValor('')
      return;
    }

    setSaved(false)
    setLoading(true)
    var dadosEnviar = {
      'chave': dicionarioChave,
      'valor': dicionarioValor
    }

    const sendData = async () =>{
      try {
        var method = 'POST';
        var url = 'admin/dicionarios';
        setSaveMsg('Dados salvos com sucesso!')
        setSaveType('success')

        var res = await new HttpMiddleware().exec( url, method, true, dadosEnviar );
        console.log(res);
        if ( res.errors ) {
          setSaveMsg(res.errors[0].message)
          setSaveType('fail')
        }
        salvarTextoComDicionario(res.id)
        setLoading(false)
        setSaved(true)
        setModal(false);
      } catch (error) {
        console.error(error.message);
        setLoading(false)
        setSaved(true)
        setSaveMsg('Erro ao salvar dados!')
        setSaveType('fail')
        setModal(false);
      }
      setDicionarioChave('')
      setDicionarioValor('')
    }

    sendData();
  }

  const openModal = ( id ) => {
    setModal(true);
  }

  const closeModal = () => {
    setDicionarioChave('')
    setDicionarioValor('')
    setModal(false);
  }

  const convertDescricao = (descricao) => {

    var descArr = descricao.split('{');
    var desc = ''

    descArr.forEach(i => {
      if ( i.indexOf('||') !== 0 ) {
        i = i.replace(i.substring(0, i.indexOf('||')), '');
      }
      desc += i;
    });

    desc = desc.split('||').join('');
    desc = desc.split('}').join('');

    console.log(desc);

    setDescricaoFormated(desc);
  }

  return (
    <div>
      { loading &&
        <div id="loading">
          <span>Carregando...</span>
        </div>
      }
      { saved &&
        <Message duration="5000" message={saveMsg} type={saveType} />
      }

      <h4>Cadastro de Mundo</h4>
      <button style={style.btnnovo} onClick={() => save()}>Salvar</button>
      <hr/>
      <div className='row'>
        <label className="col-sm-10" style={style.label}>Nome:
          <input style={style.input} type="text" value={nome || ''} onChange={e=> setNome(e.target.value)}/>
        </label>
        <label className="col-sm-2" style={style.label}>Ativo:
          <select style={style.input} value={ativo || 'true'}onChange={e=> setAtivo(e.target.value)}>
            <option value="true">Sim</option>
            <option value="false">Não</option>
          </select>
        </label>
        <div className="col-sm-12">
          <label style={style.label}>Cenário:</label>
          <Autocomplete
              value={cenarioid}
              options={cenarios}
              renderOption={esc => <Fragment>{esc.nome}</Fragment>}
              getOptionLabel={esc => typeof esc.nome === 'string'
                  || esc.nome instanceof String ? esc.nome : ""}
              style={style.inputselect}
              renderInput={params => {
                  return (
                      <TextField
                          {...params}
                          label={cenariodata.nome}
                          variant="outlined"
                          fullWidth
                      />
                  )
              }}
              onInputChange={getCenario}
          />
        </div>
        <div className="col-sm-12">
          <label style={style.label}>Posição:</label>
          <div style={{backgroundImage: 'url(' + cenariodata.image_base_64 +')', width: '500px', height: '300px', backgroundSize: 'cover'}}>
            <div style={style.cenariointerno}>
              <div style={style.cenariointernoquadro} className={`cenario-interno-quadro ${posicao === 1 ? "cenario-interno-quadro-selecionado" : ""}`} onClick={() => setPosicao(1)}>1</div>
              <div style={style.cenariointernoquadro} className={`cenario-interno-quadro ${posicao === 2 ? "cenario-interno-quadro-selecionado" : ""}`} onClick={() => setPosicao(2)}>2</div>
              <div style={style.cenariointernoquadro} className={`cenario-interno-quadro ${posicao === 3 ? "cenario-interno-quadro-selecionado" : ""}`} onClick={() => setPosicao(3)}>3</div>
              <div style={style.cenariointernoquadro} className={`cenario-interno-quadro ${posicao === 4 ? "cenario-interno-quadro-selecionado" : ""}`} onClick={() => setPosicao(4)}>4</div>
              <div style={style.cenariointernoquadro} className={`cenario-interno-quadro ${posicao === 5 ? "cenario-interno-quadro-selecionado" : ""}`} onClick={() => setPosicao(5)}>5</div>
              <div style={style.cenariointernoquadro} className={`cenario-interno-quadro ${posicao === 6 ? "cenario-interno-quadro-selecionado" : ""}`} onClick={() => setPosicao(6)}>6</div>
            </div>
          </div>
        </div>
        <div className="col-sm-6">
          <label className="col-sm-10" style={style.label}>Descrição:
            <textarea rows="13" style={style.input} type="text" value={descricao || ''} onChange={e=> setDescricao(e.target.value)}></textarea>
            <button style={style.btnnovo} onClick={() => addDicionario()} title="Adicionar ao Dicionário"><FaBook /></button>
          </label>
        </div>
        <h4 style={style.imagemaintitle}>Imagens</h4>
        <label className="col-sm-6" style={style.label}>Imagem Fundo Inicial(até 1 MB):
          <input style={style.input} type="file" onChange={e => getBase64Image(e, 'base')}/>
          <img className="image-cad" style={style.image} src={imageBase64 || ''} />
        </label>
        <label className="col-sm-6" style={style.label}>Imagem Fundo Final(até 1 MB):
          <input style={style.input} type="file" onChange={e => getBase64Image(e, 'basefinal')}/>
          <img className="image-cad" style={style.image} src={imageBase64Final || ''} />
        </label>
        <label className="col-sm-6" style={style.label}>Thumb Inicial(até 100 KB):
          <input style={style.input} type="file" onChange={e => getBase64Image(e, 'thumb')}/>
          <img className="image-cad" style={style.image} src={thumbBase64 || ''} />
        </label>
        <label className="col-sm-6" style={style.label}>Thumb Final(até 100 KB):
          <input style={style.input} type="file" onChange={e => getBase64Image(e, 'thumbfinal')}/>
          <img className="image-cad" style={style.image} src={thumbBase64Final || ''} />
        </label>
      </div>
      { modal &&
        <div className="modal-ext">
          <div className="modal-int">
            <span className="modal-close" onClick={() => closeModal()}>X</span>
            <h3>Dicionário</h3>
            <div className="row">
              <label className="col-sm-12" style={style.label}>Palavra ou termo:
                <input type="text" style={style.input} value={dicionarioChave || ''} onChange={e=> setDicionarioChave(e.target.value)}/>
              </label>
              <label className="col-sm-12" style={style.label}>Definição:
                <input type="text" style={style.input} value={dicionarioValor || ''} onChange={e=> setDicionarioValor(e.target.value)}/>
              </label>
              <button className="col-sm-10" style={style.btnAddNovaPalavra} onClick={() => enviarAoDicionario()}>Adicionar ao Dicionário</button>
            </div>
          </div>
        </div>
      }
    </div>
  );
}

const style = {
  imagemaintitle: {
    borderBottom: '1px solid',
    width: '95%',
    textAlign: 'center',
    padding: '15px'
  },
  cenarioimagem: {
    width: 'inherit',
    height: 'inherit',
    opacity: '0.8',
    float: 'left',
    position: 'absolute',
    top: '26px'
  },
  cenariointerno: {
    width: '100%',
    height: '100%'
  },
  cenariointernoquadro: {
    width: '100px',
    height: '80px',
    display: 'inline-block',
    margin: '30px',
    border: '2px solid',
    padding: '17px 41px',
    fontSize: '25px',
    background: 'rgb(0 149 135)',
    borderRadius: '4px'
  },
  label: {
    display: 'block',
    fontSize: 12
  },
  input: {
    display: 'block',
    fontSize: 15,
    padding: 3,
    width: '100%'
  },
  submit: {
    border: '1px solid',
    position: 'absolute',
    right: '4px',
    marginTop: '-40px',
    padding: '3px 10px',
    borderRadius: '0 3px 3px 0'
  },
  editbutton: {
    border: 'none',
    background: 'none'
  },
  btnnovo: {
    padding: '2px 6px 6px',
    border: '1px solid',
    margin: '5px',
    borderRadius: '3px',
    background: '#007bff',
    color: '#fff',
    position: 'absolute',
    right: 0,
    top: 0,
  },
  btnAddNovaPalavra: {
    padding: '2px 6px 6px',
    border: '1px solid',
    margin: '5px',
    borderRadius: '3px',
    background: '#007bff',
    color: '#fff',
  }
};

export default Mundo;
