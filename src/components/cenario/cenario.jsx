import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import HttpMiddleware from "../../HttpMiddleware";
import Message from '../message';

const Cenario = () => {

  const [idcad, setIdcad] = useState(0);
  const [nome, setNome] = useState('');
  const [urlimagebase, setUrlimagebase] = useState('');
  const [imageBase64, setImageBase64] = useState('')
  const [imageLogo, setImageLogo] = useState('');
  const [imageBackground, setImageBackground] = useState('')
  const [ativo, setAtivo] = useState('true');
  const [loading, setLoading] = useState(true);
  const [saved, setSaved] = useState(false);
  const [saveMsg, setSaveMsg] = useState('');
  const [saveType, setSaveType] = useState('success');

  let { id } = useParams();
  //const [posts, setPosts] = useState({ post: null, countSecrets: 0, ui: '' });

  useEffect(() => {
    /*if ( id !== 0 ) {
      var url = 'admin/escolas/' + id
      var res = new HttpMiddleware().execPromisse( url, 'GET', true, null );
      res.then((response) => {
        console.log(response.data);
        setData(response.data)
      })
    }*/
    if ( id !== '0' ) {

      setIdcad(parseInt(id));

      const fetchData = async () =>{
        try {
          var url = 'admin/cenarios/' + id
          var res = await new HttpMiddleware().exec( url, 'GET', true, null );
          setNome(res.nome)
          setAtivo(res.ativo ? 'true' : 'false')
          setImageLogo(res.image_logo)
          setImageBackground(res.image_backgound)
          console.log('carr');
        } catch (error) {
          console.error(error.message);
        }
      }
      if ( loading ) {
        fetchData();
        setLoading(false);
      }
    }
  }, []);

  const save = () => {
    setSaved(false)
    setLoading(true)
    var dadosEnviar = {
      'nome': nome,
      'ativo': ativo === 'true' ? true : false
    }

    console.log(dadosEnviar);

    const sendData = async () =>{
      try {
        var method = 'POST';
        var url = 'admin/cenarios';
        setSaveMsg('Dados salvos com sucesso!')
        setSaveType('success')
        if ( idcad && idcad !== '0' ) {
            method = 'PUT';
            url += '/' + idcad;
            setSaveMsg('Dados alterados com sucesso!')
        }

        var res = await new HttpMiddleware().exec( url, method, true, dadosEnviar );
        console.log(res);
        if ( res.errors ) {
          setSaveMsg(res.errors[0].message)
          setSaveType('fail')
        } else {
          setIdcad( res.id );
        }
        saveImg(idcad, 0);
        saveImg(idcad, 1);
        setLoading(false)
        setSaved(true)
      } catch (error) {
        console.error(error.message);
        setLoading(false)
        setSaved(true)
        setSaveMsg('Erro ao salvar dados!')
        setSaveType('fail')
      }
    }

    sendData();
  }

  const saveImg = (id, ix) => {
    setSaved(false)
    setLoading(true)
    var dadosEnviar;
    var field;
    
    if ( ix === 0 ) {
      dadosEnviar = imageLogo;
      field = 'image_logo';
    } else if ( ix === 1 ) {
        dadosEnviar = imageBackground
        field = 'image_backgound';
    }

    console.log(dadosEnviar);
    console.log(field);

    const sendData = async () =>{
      try {
        setSaveMsg('Dados salvos com sucesso!')
        setSaveType('success')

        await new HttpMiddleware().execUpload2( 'cenarios', id, field, dadosEnviar );
        setLoading(false)
        setSaved(true)
      } catch (error) {
        console.error(error?.message);
        setLoading(false)
        setSaved(true)
        setSaveMsg('Erro ao salvar dados!')
        setSaveType('fail')
      }
    }

    sendData();
  }


  const getBase64Image = (e, base) => {
    var file = e.target.files[0];

    var fileSizeMB = file.size / 1024 / 1024;

    if ( fileSizeMB > 1 ) {
      setSaved(false)
      setSaveMsg('A imagem possui um tamanho maior que 1 MB!')
      setSaveType('fail')
      setSaved(true)
      return;
    }

    if ( base === 'logo' ) {
      setImageLogo(file)
    } else if ( base === 'background' ) {
      setImageBackground(file)
    }

  }

  return (
    <div>
      { loading &&
        <div id="loading">
          <span>Carregando...</span>
        </div>
      }
      { saved &&
        <Message duration="5000" message={saveMsg} type={saveType} />
      }

      <h4>Cadastro de Cenário</h4>
      <button style={style.btnnovo} onClick={() => save()}>Salvar</button>
      <hr/>
      <form className='row'>
        <label className="col-sm-10" style={style.label}>Nome:
          <input style={style.input} type="text" value={nome || ''} onChange={e=> setNome(e.target.value)}/>
        </label>
        <label className="col-sm-2" style={style.label}>Ativo:
        <select style={style.input} value={ativo || 'true'}onChange={e=> setAtivo(e.target.value)}>
          <option value="true">Sim</option>
          <option value="false">Não</option>
        </select>
        </label>
        <label className="col-sm-12" style={style.label}>Logo(até 1 MB):
          <input style={style.input} type="file" onChange={e => getBase64Image(e, 'logo')}/>
          {imageLogo && <span className='image-cad-caption'>Logo atual</span>}
          {imageLogo && <img className="image-cad" style={style.imagelogo} src={imageLogo || ''} />}
        </label>
        <hr style={style.divisor} />
        <label className="col-sm-12" style={style.label}>Imagem de Fundo(até 1 MB):
          <input style={style.input} type="file" onChange={e => getBase64Image(e, 'background')}/>
          {imageBackground && <span className='image-cad-caption'>Imagem de Fundo atual</span>}
          {imageBackground && <img className="image-cad" style={style.image} src={imageBackground || ''} />}
        </label>
      </form>
    </div>
  );
}

const style = {
  label: {
    display: 'block',
    fontSize: 12
  },
  divisor: {
    width: '100%'
  },
  imagelogo: {
    width: 200
  },
  input: {
    display: 'block',
    fontSize: 15,
    padding: 3,
    width: '100%'
  },
  submit: {
    border: '1px solid',
    position: 'absolute',
    right: '4px',
    marginTop: '-40px',
    padding: '3px 10px',
    borderRadius: '0 3px 3px 0'
  },
  editbutton: {
    border: 'none',
    background: 'none'
  },
  btnnovo: {
    padding: '2px 6px 6px',
    border: '1px solid',
    margin: '5px',
    borderRadius: '3px',
    background: '#007bff',
    color: '#fff',
    position: 'absolute',
    right: 0,
    top: 0,
  }
};

export default Cenario;
