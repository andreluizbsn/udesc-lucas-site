import React, { Component } from "react";
import { Navigate  } from 'react-router'
import { FaHome, FaChalkboardTeacher, FaChild, FaSchool, FaUserFriends, FaBorderNone, FaBorderStyle, FaBookOpen, FaGamepad } from 'react-icons/fa';

const userprofileback = process.env.PUBLIC_URL + '/userprofileback.png'

class Aside extends Component {
  constructor(props) {
    super(props);
    this.state = {redirect: null, userID: 0, userName: ''};
  }

  componentDidMount() {
    if ( sessionStorage.getItem('user') ) {
      let user = JSON.parse(sessionStorage.getItem('user'));
      this.setState({'userID': user.id, 'userName': user.nome});
    }
  }

  render() {
    if ( !sessionStorage.getItem('user') ) {
      this.setState({redirect: '/login'});
    }
    if (this.state.redirect === '/login') {
      return <Navigate  to="/login"/>
    }
    return (
      <div>
        <div style={style.profileblock}>
          <img src={process.env.PUBLIC_URL + '/user.png'} style={style.imguser} alt="Logo"/>
          <a href="/login" style={style.nameuser}>{this.state.userName}</a>
        </div>
        <nav style={style.navbarclass}>
          <a style={style.navbarclasslink} href="/"><FaHome/><span style={style.navbarclasslinkspan}>Dashboard</span></a>
          <a style={style.navbarclasslink} href="/escolas"><FaSchool/><span style={style.navbarclasslinkspan}>Escolas</span></a>
          <a style={style.navbarclasslink} href="/professores"><FaChalkboardTeacher/><span style={style.navbarclasslinkspan}>Professores</span></a>
          <a style={style.navbarclasslink} href="/alunos"><FaChild/><span style={style.navbarclasslinkspan}>Alunos</span></a>
          <a style={style.navbarclasslink} href="/usuarios"><FaUserFriends/><span style={style.navbarclasslinkspan}>Usuários</span></a>
          <a style={style.navbarclasslink} href="/cenarios"><FaBorderNone/><span style={style.navbarclasslinkspan}>Cenários</span></a>
          <a style={style.navbarclasslink} href="/mundos"><FaBorderStyle/><span style={style.navbarclasslinkspan}>Mundos</span></a>
          <a style={style.navbarclasslink} href="/jogos"><FaGamepad/><span style={style.navbarclasslinkspan}>Jogos</span></a>
          <a style={style.navbarclasslink} href="/dicionarios"><FaBookOpen/><span style={style.navbarclasslinkspan}>Dicionário</span></a>
        </nav>
      </div>
    );
  }
}


const style = {
  profileblock: {
    display: 'block',
    width: '100%',
    height: 100,
    textAlign: 'center',
    borderBottom: '1px solid',
    paddingTop: 15,
    paddingBottom: 15,
    backgroundImage: `url(${userprofileback})`,
    backgroundPosition:'center',
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'cover'
  },
  imguser: {
    width: 'auto',
  },
  nameuser: {
    fontSize: 12,
    display: 'block',
    textAlign: 'center',
    color: '#fff',
    textDecoration: 'none',
  },
  nameuserhover: {
    color: '#595959'
  },
  navbarclass: {
    display: 'block'
  },
  navbarclasslink: {
    display: 'flex',
    width: '100%',
    color: '#fff',
    alignItems: 'center',
    paddingTop: 5,
    paddingBottom: 5,
    paddingLeft: 5,
    textDecoration: 'none',
    borderBottom: '1px solid #212529',
    '&:hover': {
      border: '1px solid black'
    },
  },
  navbarclasslinkspan: {
    marginLeft: 5
  },
  img: {
    width: 'auto',
    marginBottom: 10
  },
  input: {
    display: 'block',
    fontSize: 15,
    width: '100%'
  },
  submit: {
    background: '#069633',
    color: '#033135',
    borderRadius: 2,
    borderWidth: 1,
    borderColor: '#fff',
    "&:hover": {
      boxShadow: "0px 0px 3px #033135"
    }
  },


};

export default Aside;
