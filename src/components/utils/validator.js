class Validator {

  fields( data ) {

    if ( !data.isArray() ) {
      data = [
        data;
      ]
    }

    var ret = [];

    data(d => {
      if ( d.type === 'INTEGER' ) {

      } else if ( d.type === 'STRING' ) {

        if ( this.stringVal( d.value, d.nullable || true, d.minsize || 0, d.maxsize || 0 ) !== null ) {
          ret.push({
            'field': d.field,
            'message': this.stringVal( d.value, d.nullable || true, d.minsize || 0, d.maxsize || 0 )
          })
        }

      } else if ( d.type === 'BOOLEAN' ) {
        if( this.booleanVal( d.value ) === null || this.booleanVal( d.value ) === 'undefined' ) {
            ret.push({
              'field': d.field,
              'message': 'O campo ' + d.name + ' possui valor incorreto!'
            })
        }
      }
    });

    return ret;
  }

  booleanVal( val ) {
    if ( val === 0 ) {
      return false;
    } else if ( val === 1 ) {
      return true;
    } else if ( val.toLowerCase() === 'true' ) {
      return true;
    } else if ( val.toLowerCase() === 'false' ) {
      return false;
    }
    return null;
  }

  stringVal( val, nullable minsize, maxsize ) {
    var retorno;
    if ( nullable && ( val === null || val === undefined || val === '' ) ) {
      retorno += 'O campo não pode ser vazio! '
    }

    if ( val.lenght < minsize ) {
      retorno += 'O campo não pode conter mais que ' + maxsize + ' caracteres! '
    }

    if ( val.lenght > maxsize ) {
      retorno += 'O campo não pode conter mais que ' + maxsize + ' caracteres! '
    }

    return retorno;
  }

}

export default Validator;
