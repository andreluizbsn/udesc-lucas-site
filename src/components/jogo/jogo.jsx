import React, { useState, useEffect, Fragment } from 'react';
import { useParams } from 'react-router-dom';
import HttpMiddleware from "../../HttpMiddleware";
import Message from '../message';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { FaEdit, FaCheck, FaTrash, FaPlus, FaSave, FaGamepad, FaTimes } from 'react-icons/fa';
import TextDict from '../utils/TextDict';

const Jogo = () => {

  const [idcad, setIdcad] = useState(0);
  const [nome, setNome] = useState('');
  const [posicao, setPosicao] = useState(0);
  const [descricao, setDescricao] = useState('');
  const [ativo, setAtivo] = useState('true');
  const [grupo, setGrupo] = useState(0);
  const [tipo, setTipo] = useState('QUIZ');
  const [usuarioid, setUsuarioid] = useState(0);

  const [idctrl, setIdctrl] = useState(-1)

  const [items, setItems] = useState([]);
  const [itemsRun1, setItemsRun1] = useState([]);
  const [itemsRun2, setItemsRun2] = useState([]);

  const [iditem, setIditem] = useState(0);
  const [descricaoitem, setDescricaoitem] = useState('');
  const [verdadeiroitem, setVerdadeiroitem] = useState('true');

  const [mundos, setMundos] = useState([]);
  const [mundoid, setMundoid] = useState(0);
  const [mundodata, setMundodata] = useState({});

  const [dicionarios, setDicionarios] = useState([]);

  const [modal, setModal] = useState(false);
  const [executarGame, setExecutarGame] = useState(false);
  const [registroExcluir, setRegistroExcluir] = useState('');
  const [registroExcluirId, setRegistroExcluirId] = useState(0);

  const [loading, setLoading] = useState(true);
  const [saved, setSaved] = useState(false);
  const [saveMsg, setSaveMsg] = useState('');
  const [saveType, setSaveType] = useState('success');

  let { id } = useParams();
  //const [posts, setPosts] = useState({ post: null, countSecrets: 0, ui: '' });

  useEffect(() => {

    loadingMundos();
    loadingDicionarios();

    if ( id !== '0' ) {

      setIdcad(parseInt(id));

      const fetchData = async () =>{
        try {
          var url = 'admin/jogos/' + id
          var res = await new HttpMiddleware().exec( url, 'GET', true, null );
          setNome(res.nome)
          setAtivo(res.ativo ? 'true' : 'false')
          setMundoid(res.mundo_id)
          setMundodata(res.mundo)
          setPosicao(res.posicao)
          setDescricao(res.descricao)
          setGrupo(res.grupo)
          setTipo(res.tipo)
          setItems(res.items)
          setUsuarioid(res.user_id)


        } catch (error) {
          console.error(error.message);
        }
      }
      if ( loading ) {
        fetchData();
        setLoading(false);
      }
    } else {
      var users = JSON.parse(sessionStorage.getItem('user'));
      setUsuarioid(users.id)
    }
  }, []);

  const loadingMundos = () => {
    const lEsc = async () =>{
      try {
        var urlEsc = 'admin/mundos?no_paginate=true';
        var resEsc = await new HttpMiddleware().exec( urlEsc, 'GET', true, null );

        setMundos(resEsc)

      } catch (error) {
        console.error(error.message);
      }
    }
    lEsc();
  }

  const loadingDicionarios = () => {
    const lEsc = async () =>{
      try {
        var urlEsc = 'admin/dicionarios?no_paginate=true';
        var resEsc = await new HttpMiddleware().exec( urlEsc, 'GET', true, null );

        setDicionarios(resEsc)

        console.log(resEsc);

      } catch (error) {
        console.error(error.message);
      }
    }
    lEsc();
  }

  const getMundo = (event,val) => {
      mundos.forEach(i => {
        if ( i.nome === val ) {
          setMundoid(i.id)
          setMundodata(i)
        }
      });

  }

  const save = () => {
    setSaved(false)
    setLoading(true)
    var dadosEnviar = {
      'nome': nome,
      'mundo_id': mundoid,
      'descricao': descricao,
      'ativo': ativo === 'true' ? true : false,
      'posicao': posicao,
      'grupo': grupo,
      'tipo': tipo,
      'user_id': usuarioid,
      'items': items
    }

    console.log(dadosEnviar);

    //return;

    const sendData = async () =>{
      try {
        var method = 'POST';
        var url = 'admin/jogos';
        setSaveMsg('Dados salvos com sucesso!')
        setSaveType('success')
        console.log(idcad);
        if ( idcad && idcad !== '0' ) {
            method = 'PUT';
            url += '/' + idcad;
            setSaveMsg('Dados alterados com sucesso!')
        }

        var res = await new HttpMiddleware().exec( url, method, true, dadosEnviar );
        console.log(res);
        if ( res.errors ) {
          setSaveMsg(res.errors[0].message)
          setSaveType('fail')
        } else {
          setIdcad( res.id );
          setItems(res.items)
        }
        setLoading(false)
        setSaved(true)
      } catch (error) {
        console.error(error.message);
        setLoading(false)
        setSaved(true)
        setSaveMsg('Erro ao salvar dados!')
        setSaveType('fail')
      }
    }

    sendData();
  }

  const getBase64Image = (e, base) => {
    var file = e.target.files[0];

    console.log(file);
    console.log(base);

    var fileSizeMB = file.size / 1024;


    if ( base === 'base' || base === 'basefinal' ) {
      fileSizeMB = fileSizeMB / 1024;
      if ( fileSizeMB > 1 ) {
        setSaved(false)
        setSaveMsg('A imagem possui um tamanho maior que 1 MB!')
        setSaveType('fail')
        setSaved(true)
        return;
      }
    } else {
      if ( fileSizeMB > 100 ) {
        setSaved(false)
        setSaveMsg('A imagem possui um tamanho maior que 100 KB!')
        setSaveType('fail')
        setSaved(true)
        return;
      }
    }



    let reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = function () {
        console.log(reader.result);
        /*setUrlimagebase(file.name)
        if ( base === 'base' ) {
          setImageBase64(reader.result)
        } else if ( base === 'basefinal' ) {
          setImageBase64Final(reader.result)
        } else if ( base === 'thumb' ) {
          setThumbBase64(reader.result)
        } else if ( base === 'thumbfinal' ) {
          setThumbBase64Final(reader.result)
        }*/
    };
    reader.onerror = function (error) {
        console.log('Error: ', error);
    };

  }

  const editItem = ( dat ) => {
    console.log(dat);
    setDescricaoitem(dat.dado);
    setIditem(dat.id);
    setVerdadeiroitem(dat.verdadeiro ? 'true' : 'false');
  }

  const salvarItem = () => {
    var its = items;
    if ( iditem === 0 ) {
      its.push({
        'id': idctrl,
        'dado': descricaoitem,
        'verdadeiro': ( verdadeiroitem === 'true' ? true : false )
      })
      setIdctrl(idctrl - 1)
    } else {
      its.forEach(i => {
        if ( i.id === iditem ) {
          i.dado = descricaoitem;
          i.verdadeiro = ( verdadeiroitem === 'true' ? true : false );
        }
      });
    }
    setItems(its);
    setIditem(0);
    setDescricaoitem('');
    setVerdadeiroitem('true');
  }

  const openModal = ( id ) => {
    items.forEach(i => {
      if ( i.id === id ) {
        setRegistroExcluir(i.dado)
        setRegistroExcluirId(id)
      }
    });

    setModal(true);
  }

  const closeModal = () => {
    setModal(false);
    setExecutarGame(false)
    setRegistroExcluir('')
    setRegistroExcluirId(0)
  }

  const deleteItem = () => {

    var newArrItem = [];

    items.forEach(i => {
      if ( i.id !== registroExcluirId ) {
        newArrItem.push( i );
      }
    });

    setItems( newArrItem );
    setModal(false);
    setRegistroExcluir('')
    setRegistroExcluirId(0)

  }

  const runGame = () => {

    var itemVerdadeiro;
    var qtdeItens = items.lenght;

    function getRandomInt(max) {
      return Math.floor(Math.random() * max);
    }

    if ( qtdeItens < 4 ) {
      setSaveMsg('É preciso ter pelo menos 4 itens cadastrados para QUIZ!')
      setSaved(true);
      return;
    } else {
      var verdadeiros = [];
      var falsos = [];

      items.forEach(i => {
        i.show_opt = false;
        if ( i.verdadeiro ) {
          verdadeiros.push( i )
        } else {
          falsos.push( i )
        }
      });

      if ( verdadeiros.length === 0) {
        setSaveMsg('É preciso ter pelo menos 1 item cadastrados para QUIZ como verdadeiro!')
        setSaved(true);
        return;
      } else if ( verdadeiros.length === 1 ) {
        itemVerdadeiro = verdadeiros[0];
      } else {
        var ixVerd = getRandomInt(verdadeiros.length)
        itemVerdadeiro = verdadeiros[ixVerd];
      }

      if ( falsos.length < 3) {
        setSaveMsg('É preciso ter pelo menos 3 itens cadastrados para QUIZ como falso!')
        setSaved(true);
        return;
      } else if ( falsos.length > 3 ) {

        var ixFalsos = [];
        var ixFalso;
        var hasReg = false;

        while ( ixFalsos.length < 3 ) {
          ixFalso = getRandomInt(falsos.length)

          ixFalsos.forEach( i => {
            if ( i === ixFalso ) {
              hasReg = true;
            }
          });

          if ( !hasReg ) {
            ixFalsos.push( ixFalso );
          }

          hasReg = false;

        }

        var newFalsos = [];

        ixFalsos.forEach(i=> {
          newFalsos.push( falsos[i] );
        });

        falsos = newFalsos;

      }

    }

    var columnTrue = getRandomInt(2)
    var lineTrue = getRandomInt(2)

    var c1 = []
    var c2 = []
    var ixF = 0;
    var lista = [];

    for (var i = 0; i < 2; i++) {
      for (var f = 0; f < 2; f++) {
        if ( i === columnTrue && f === lineTrue ) {
          lista.push( itemVerdadeiro )
        } else {
          lista.push( falsos[ixF] )
          ixF += 1;
        }
      }
    }

    setItemsRun1( lista.slice(0, 2) )
    setItemsRun2( lista.slice(2, 4) )

    setExecutarGame(true)

  }

  const validaOpcao = ( e ) => {
    if ( e.verdadeiro ) {
      alert('Acertou');
    } else {
      alert('Errou');
    }

    var list1 = itemsRun1;
    var list2 = itemsRun2;

    list1.forEach(i => {
      i.show_opt = true;
    });
    list2.forEach(i => {
      i.show_opt = true;
    });

    setItemsRun1( list1 );
    setItemsRun2( list2 );

    console.log(itemsRun1);
    console.log(itemsRun2);
  }

  return (
    <div>
      { loading &&
        <div id="loading">
          <span>Carregando...</span>
        </div>
      }
      { saved &&
        <Message duration="5000" message={saveMsg} type={saveType} />
      }

      <h4>Cadastro de Jogo</h4>
      <button style={style.btnnovo} onClick={() => save()}>Salvar</button>{ idcad !== 0 && <button title="Executar o Game" style={style.btngamerun} onClick={() => runGame()}><FaGamepad /></button>}
      <hr/>
      <form className='row'>
        <label className="col-sm-10" style={style.label}>Nome:
          <input style={style.input} type="text" value={nome || ''} onChange={e=> setNome(e.target.value)}/>
        </label>
        <label className="col-sm-2" style={style.label}>Ativo:
          <select style={style.input} value={ativo || 'true'}onChange={e=> setAtivo(e.target.value)}>
            <option value="true">Sim</option>
            <option value="false">Não</option>
          </select>
        </label>
        <div className="col-sm-4">
          <label style={style.label}>Mundo:</label>
          <Autocomplete
              value={mundoid}
              options={mundos}
              renderOption={esc => <Fragment>{esc.nome}</Fragment>}
              getOptionLabel={esc => typeof esc.nome === 'string'
                  || esc.nome instanceof String ? esc.nome : ""}
              style={style.inputselect}
              renderInput={params => {
                  return (
                      <TextField
                          {...params}
                          label={mundodata.nome}
                          variant="outlined"
                          fullWidth
                      />
                  )
              }}
              onInputChange={getMundo}
          />
        </div>
        <label className="col-sm-5" style={style.label}>Grupo:
          <select style={style.input} value={grupo || 'true'}onChange={e=> setGrupo(e.target.value)}>
            <option value="1">Fundamental I</option>
            <option value="2">Fundamental II</option>
          </select>
        </label>
        <label className="col-sm-3" style={style.label}>Tipo:
          <select style={style.input} value={tipo || 'true'}onChange={e=> setTipo(e.target.value)}>
            <option value="QUIZ">Quiz</option>
            <option value="MEMORIA">Jogo Memória</option>
          </select>
        </label>
        <div className="col-sm-6">
          <label className="col-sm-12" style={style.label}>Descrição/Pergunta:
            <textarea rows="6" style={style.input} type="text" value={descricao || ''} onChange={e=> setDescricao(e.target.value)}></textarea>
          </label>
        </div>
      </form>
      <div className="row">
        <div className="col-sm-12">
          <h6 style={style.itemtitle}>Itens do Jogo</h6>
          <button style={style.btnnovoitem}><FaPlus /></button>
          <div className="row">
            <label className="col-sm-8" style={style.label}>Descrição:
              <input style={style.input} type="text" value={descricaoitem || ''} onChange={e=> setDescricaoitem(e.target.value)}/>
            </label>
            <label className="col-sm-2" style={style.label}>Verdadeira?:
              <select style={style.input} value={verdadeiroitem || 'true'} onChange={e=> setVerdadeiroitem(e.target.value)}>
                <option value="true">Sim</option>
                <option value="false">Não</option>
              </select>
            </label>
            <div className="col-sm-2">
              <span style={style.label}>Salvar</span>
              <button style={style.btnedit} onClick={() => salvarItem()}><FaSave /></button>
            </div>
          </div>
          <table id="table">
            <thead>
              <tr>
                <th>#</th>
                <th>Descrição</th>
                <th>Verdadeiro?</th>
                <th>Ações</th>
              </tr>
            </thead>
            <tbody>
              {items.map((e) => (
                <tr key={e.id}>
                  <td>{e.id}</td>
                  <td>{e.dado}</td>
                  <td>{ e.verdadeiro ? <FaCheck /> : ''}</td>
                  <td><button style={style.btnedit} onClick={() => editItem(e)}><FaEdit /></button><button style={style.btndelete} onClick={() => openModal(e.id)}><FaTrash /></button></td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
      { modal &&
        <div className="modal-ext">
          <div className="modal-int">
            <span className="modal-close" onClick={() => closeModal()}>X</span>
            <h3>Tem certeza que deseja excluir esse registro?</h3>
            <p>{registroExcluir}</p>
            <div className="modal-btn">
              <button className="modal-buttom-nao" onClick={() => closeModal()}>Não</button><button className="modal-buttom-sim" onClick={() => deleteItem()}>Sim</button>
            </div>
          </div>
        </div>
      }
      { executarGame &&
        <div className="modal-ext">
          <div className="modal-int">
            <span className="modal-close" onClick={() => closeModal()}>X</span>
            <div style={{backgroundImage: 'url(' + process.env.PUBLIC_URL + '/cellphone.png'+')', width: '815px', height: '400px', backgroundSize: 'cover', display: 'flex', flexDirection: 'column'}}>
              <div style={{margin: 50, padding: 15, width: '87%', border: '3px solid', borderRadius: '10px', marginBottom: 10}}>
                <h4>{descricao}</h4>
              </div>
              <div style={{margin: 50, padding: 15, width: '87%', border: '3px solid', borderRadius: '10px', height: 215, marginTop: 10, display: 'flex', justifyContent: 'space-between'}}>
                <div style={{width: '49%', height: '100%', display: 'flex', flexDirection: 'column'}}>
                  {itemsRun1.map((e) => (
                    <button key={e.id} style={{padding: 15, width: '100%', borderRadius: '15px', marginBottom: 15}} onClick={ () => validaOpcao(e) }>{e.dado} {e.verdadeiro && e.show_opt && <FaCheck />}{!e.verdadeiro && e.show_opt && <FaTimes />}</button>
                  ))}
                </div>
                <div style={{width: '49%', height: '100%', display: 'flex', flexDirection: 'column'}}>
                  {itemsRun2.map((e) => (
                    <button key={e.id} style={{padding: 15, width: '100%', borderRadius: '15px', marginBottom: 15}} onClick={ () => validaOpcao(e) }>{e.dado} {e.verdadeiro && e.show_opt && <FaCheck />}{!e.verdadeiro && e.show_opt && <FaTimes />}</button>
                  ))}
                </div>
              </div>
            </div>
          </div>
        </div>
      }
    </div>
  );
}

const style = {
  itemtitle: {
    borderTop: '1px solid rgba(0,0,0,.2)',
    paddingTop: '10px',
    marginTop: '10px'
  },
  btnedit: {
    background: '#007bff',
    padding: '2px 6px 6px',
    border: '1px solid',
    borderRadius: '3px',
    color: '#fff'
  },
  btndelete: {
    background: 'rgb(245 50 50)',
    padding: '2px 6px 6px',
    border: '1px solid',
    borderRadius: '3px',
    color: '#fff'
  },
  imagemaintitle: {
    borderBottom: '1px solid',
    width: '95%',
    textAlign: 'center',
    padding: '15px'
  },
  cenarioimagem: {
    width: 'inherit',
    height: 'inherit',
    opacity: '0.8',
    float: 'left',
    position: 'absolute',
    top: '26px'
  },
  cenariointerno: {
    width: '100%',
    height: '100%'
  },
  cenariointernoquadro: {
    width: '100px',
    height: '80px',
    display: 'inline-block',
    margin: '30px',
    border: '2px solid',
    padding: '17px 41px',
    fontSize: '25px',
    background: 'rgb(0 149 135)',
    borderRadius: '4px'
  },
  label: {
    display: 'block',
    fontSize: 12
  },
  input: {
    display: 'block',
    fontSize: 15,
    padding: 3,
    width: '100%'
  },
  submit: {
    border: '1px solid',
    position: 'absolute',
    right: '4px',
    marginTop: '-40px',
    padding: '3px 10px',
    borderRadius: '0 3px 3px 0'
  },
  editbutton: {
    border: 'none',
    background: 'none'
  },
  btngamerun: {
    padding: '2px 6px 6px',
    border: '1px solid',
    margin: '5px',
    borderRadius: '3px',
    background: 'rgb(35 195 106)',
    color: '#fff',
    position: 'absolute',
    right: 0,
    top: 0,
  },
  btnnovo: {
    padding: '2px 6px 6px',
    border: '1px solid',
    margin: '5px',
    borderRadius: '3px',
    background: '#007bff',
    color: '#fff',
    position: 'absolute',
    right: '32px',
    top: 0,
  },
  btnnovoitem: {
    padding: '2px 6px 6px',
    border: '1px solid',
    margin: '5px',
    borderRadius: '3px',
    background: '#007bff',
    color: '#fff',
    position: 'absolute',
    right: '8px',
    top: '8px',
  }
};

export default Jogo;
