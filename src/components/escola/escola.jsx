import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import HttpMiddleware from "../../HttpMiddleware";
import Message from '../message';

var camposValidacao = [
  {
    'field': 'nome',
    'name': 'nome',
    'type': 'string',
    'minsize': 10
  }
]

const Escola = () => {

  const [idcad, setIdcad] = useState(0);
  const [nome, setNome] = useState('');
  const [sigla, setSigla] = useState('');
  const [cnpj, setCnpj] = useState('');
  const [loading, setLoading] = useState(true);
  const [saved, setSaved] = useState(false);
  const [saveMsg, setSaveMsg] = useState('');
  const [saveType, setSaveType] = useState('success');

  let { id } = useParams();
  //const [posts, setPosts] = useState({ post: null, countSecrets: 0, ui: '' });

  useEffect(() => {
    /*if ( id !== 0 ) {
      var url = 'admin/escolas/' + id
      var res = new HttpMiddleware().execPromisse( url, 'GET', true, null );
      res.then((response) => {
        console.log(response.data);
        setData(response.data)
      })
    }*/
    if ( id !== '0' ) {

      setIdcad(parseInt(id));

      const fetchData = async () =>{
        try {
          var url = 'admin/escolas/' + id
          var res = await new HttpMiddleware().exec( url, 'GET', true, null );
          setNome(res.nome)
          setSigla(res.sigla)
          setCnpj(res.cnpj)
          console.log('carr');
        } catch (error) {
          console.error(error.message);
        }
      }
      if ( loading ) {
        fetchData();
        setLoading(false);
      }
    }
  }, []);

  const save = () => {
    setSaved(false)
    setLoading(true)
    var dadosEnviar = {
      'nome': nome,
      'sigla': sigla,
      'cnpj': cnpj
    }

    console.log(dadosEnviar);

    const sendData = async () =>{
      try {
        var method = 'POST';
        var url = 'admin/escolas';
        setSaveMsg('Dados salvos com sucesso!')
        setSaveType('success')
        if ( idcad !== '0' ) {
            method = 'PUT';
            url += '/' + idcad;
            setSaveMsg('Dados alterados com sucesso!')
        }

        var res = await new HttpMiddleware().exec( url, method, true, dadosEnviar );
        console.log(res);
        if ( res.errors ) {
          setSaveMsg(res.errors[0].message)
          setSaveType('fail')
        } else {
          setIdcad( res.id );
        }
        setLoading(false)
        setSaved(true)
      } catch (error) {
        console.error(error.message);
        setLoading(false)
        setSaved(true)
        setSaveMsg('Erro ao salvar dados!')
        setSaveType('fail')
      }
    }

    sendData();
  }

  return (
    <div>
      { loading &&
        <div id="loading">
          <span>Carregando...</span>
        </div>
      }
      { saved &&
        <Message duration="5000" message={saveMsg} type={saveType} />
      }

      <h4>Cadastro de Escolas</h4>
      <button style={style.btnnovo} onClick={() => save()}>Salvar</button>
      <hr/>
      <form className='row'>
        <label className="col-sm-12" style={style.label}>Nome:
          <input style={style.input} type="text" value={nome || ''} onChange={e=> setNome(e.target.value)}/>
        </label>
        <label className="col-sm-6" style={style.label}>Sigla:
          <input style={style.input} type="text" value={sigla || ''} onChange={e=> setSigla(e.target.value)}/>
        </label>
        <label className="col-sm-6" style={style.label}>CNPJ:
          <input style={style.input} type="text" value={cnpj || ''}onChange={e=> setCnpj(e.target.value)}/>
        </label>
      </form>
    </div>
  );
}

const style = {
  label: {
    display: 'block',
    fontSize: 12
  },
  input: {
    display: 'block',
    fontSize: 15,
    padding: 3,
    width: '100%'
  },
  submit: {
    border: '1px solid',
    position: 'absolute',
    right: '4px',
    marginTop: '-40px',
    padding: '3px 10px',
    borderRadius: '0 3px 3px 0'
  },
  editbutton: {
    border: 'none',
    background: 'none'
  },
  btnnovo: {
    padding: '2px 6px 6px',
    border: '1px solid',
    margin: '5px',
    borderRadius: '3px',
    background: '#007bff',
    color: '#fff',
    position: 'absolute',
    right: 0,
    top: 0,
  }
};

export default Escola;
