import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import HttpMiddleware from "../../HttpMiddleware";
import Message from '../message';

const Cenario = () => {

  const [idcad, setIdcad] = useState(0);
  const [chave, setChave] = useState('');
  const [valor, setValor] = useState('');
  const [loading, setLoading] = useState(true);
  const [saved, setSaved] = useState(false);
  const [saveMsg, setSaveMsg] = useState('');
  const [saveType, setSaveType] = useState('success');

  let { id } = useParams();
  //const [posts, setPosts] = useState({ post: null, countSecrets: 0, ui: '' });

  useEffect(() => {
    /*if ( id !== 0 ) {
      var url = 'admin/escolas/' + id
      var res = new HttpMiddleware().execPromisse( url, 'GET', true, null );
      res.then((response) => {
        console.log(response.data);
        setData(response.data)
      })
    }*/
    if ( id !== '0' ) {

      setIdcad(parseInt(id));

      const fetchData = async () =>{
        try {
          var url = 'admin/dicionarios/' + id
          var res = await new HttpMiddleware().exec( url, 'GET', true, null );
          setChave(res.chave)
          setValor(res.valor)
          console.log('carr');
        } catch (error) {
          console.error(error.message);
        }
      }
      if ( loading ) {
        fetchData();
        setLoading(false);
      }
    }
  }, []);

  const save = () => {
    setSaved(false)
    setLoading(true)
    var dadosEnviar = {
      'chave': chave,
      'valor': valor
    }

    console.log(dadosEnviar);

    const sendData = async () =>{
      try {
        var method = 'POST';
        var url = 'admin/dicionarios';
        setSaveMsg('Dados salvos com sucesso!')
        setSaveType('success')
        if ( idcad !== 0 ) {
            method = 'PUT';
            url += '/' + idcad;
            setSaveMsg('Dados alterados com sucesso!')
        }

        var res = await new HttpMiddleware().exec( url, method, true, dadosEnviar );
        console.log(res);
        if ( res.errors ) {
          setSaveMsg(res.errors[0].message)
          setSaveType('fail')
        } else {
          setIdcad( res.id );
        }
        setLoading(false)
        setSaved(true)
      } catch (error) {
        console.error(error.message);
        setLoading(false)
        setSaved(true)
        setSaveMsg('Erro ao salvar dados!')
        setSaveType('fail')
      }
    }

    sendData();
  }

  return (
    <div>
      { loading &&
        <div id="loading">
          <span>Carregando...</span>
        </div>
      }
      { saved &&
        <Message duration="5000" message={saveMsg} type={saveType} />
      }

      <h4>Cadastro de Dicionário</h4>
      <button style={style.btnnovo} onClick={() => save()}>Salvar</button>
      <hr/>
      <form className='row'>
        <label className="col-sm-12" style={style.label}>Chave:
          <input style={style.input} type="text" value={chave || ''} onChange={e=> setChave(e.target.value)}/>
        </label>
        <label className="col-sm-12" style={style.label}>Valor:
          <input style={style.input} type="text" value={valor || ''} onChange={e=> setValor(e.target.value)}/>
        </label>
      </form>
    </div>
  );
}

const style = {
  label: {
    display: 'block',
    fontSize: 12
  },
  input: {
    display: 'block',
    fontSize: 15,
    padding: 3,
    width: '100%'
  },
  submit: {
    border: '1px solid',
    position: 'absolute',
    right: '4px',
    marginTop: '-40px',
    padding: '3px 10px',
    borderRadius: '0 3px 3px 0'
  },
  editbutton: {
    border: 'none',
    background: 'none'
  },
  btnnovo: {
    padding: '2px 6px 6px',
    border: '1px solid',
    margin: '5px',
    borderRadius: '3px',
    background: '#007bff',
    color: '#fff',
    position: 'absolute',
    right: 0,
    top: 0,
  }
};

export default Cenario;
