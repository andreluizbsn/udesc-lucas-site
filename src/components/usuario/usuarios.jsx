import React, { Component } from "react";
import HttpMiddleware from "../../HttpMiddleware";
import { FaSearch, FaEdit, FaPlus, FaAngleDoubleLeft, FaAngleDoubleRight, FaAngleLeft, FaAngleRight } from 'react-icons/fa';

class Usuarios extends Component {

  constructor(props) {
    super(props);
    this.state = {dados: [], pagination: {}, filtro: '', page: 1};
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    //this.navegacao = this.navegacao.bind(this);
  }

  async componentDidMount() {

    await this.carregarDados();

  }

  handleChange(event) {
    console.log(this.state);
    this.setState({filtro: event.target.value, page: 1});
  }

  async carregarDados() {
    var url = 'admin/users?page=' + this.state.page;

    if ( this.state.filtro && this.state.filtro !== '' ) {
      url += '&filtro=' + this.state.filtro;
    }

    var dataRet = await new HttpMiddleware().exec( url, 'GET', true, null )

    this.setState({
      'dados': dataRet.data,
      'pagination': dataRet.pagination,
    })
  }

  async handleSubmit(e) {

    e.preventDefault();

    await this.carregarDados();

  }

  async navegacao( posicao ) {
    var pageNow;
    var pageLast;
    if ( posicao === 'first' ){
      this.setState({page: 1});
    } else if ( posicao === 'back' ){
      pageNow = this.state.pagination.page;
      if ( pageNow === 1 ) {
        this.setState({page: 1});
      } else {
        pageNow -= 1;
        this.setState({page: pageNow});
      }
    } else if ( posicao === 'next' ) {
      pageNow = this.state.pagination.page;
      if ( pageNow === this.state.pagination.lastPage ) {
        this.setState({page: pageNow});
      } else {
        pageNow += 1;
        this.setState({page: pageNow});
      }
    } else if ( posicao === 'last' ) {
      pageLast = this.state.pagination.lastPage;
      this.setState({page: pageLast});
    }
    await this.carregarDados();
  }

  render() {
    return (
      <div>
        <h4>Lista de Alunos</h4>
        <a style={style.btnnovo} href="/usuario/0"><FaPlus /></a>
        <hr/>
        <form onSubmit={this.handleSubmit}>
          <label style={style.label}>Buscar:
            <input style={style.input} type="text" value={this.state.filtro} onChange={this.handleChange} />
          </label>
          <button style={style.submit} type="submit"><FaSearch /></button>
        </form>
        <hr/>
        <span>{this.state.pagination.total} registro(s)</span>
        <table id="table">
          <thead>
            <tr>
              <th></th>
              <th>#</th>
              <th>Nome</th>
              <th>E-Mail</th>
              <th>Tipo</th>
            </tr>
          </thead>
          <tbody>
            {this.state.dados.map((e) => (
              <tr key={e.id}>
                <td><a style={style.editbutton} href={'usuario/' + e.id}><FaEdit /></a></td>
                <td>{e.id}</td>
                <td>{e.nome}</td>
                <td>{e.login}</td>
                <td>{e.tipo}</td>
              </tr>
            ))}
          </tbody>
        </table>
        <div id="group-button">
          <button onClick={() => this.navegacao('first')}><FaAngleDoubleLeft /></button>
          <button onClick={() => this.navegacao('back')}><FaAngleLeft /></button>
          <span>Página {this.state.pagination.page}</span>
          <button onClick={() => this.navegacao('next')}><FaAngleRight /></button>
          <button onClick={() => this.navegacao('last')}><FaAngleDoubleRight /></button>
        </div>
      </div>
    );
  }
}

const style = {
  label: {
    display: 'block',
    fontSize: 12
  },
  input: {
    display: 'block',
    fontSize: 15,
    padding: 3,
    width: '100%'
  },
  submit: {
    border: '1px solid',
    position: 'absolute',
    right: '4px',
    marginTop: '-40px',
    padding: '3px 10px',
    borderRadius: '0 3px 3px 0'
  },
  editbutton: {
    border: 'none',
    background: 'none'
  },
  btnnovo: {
    padding: '2px 6px 6px',
    border: '1px solid',
    margin: '5px',
    borderRadius: '3px',
    background: '#007bff',
    color: '#fff',
    position: 'absolute',
    right: 0,
    top: 0,
  }
};

export default Usuarios;
