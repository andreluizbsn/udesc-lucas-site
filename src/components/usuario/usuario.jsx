import React, { useState, useEffect, Fragment } from 'react';
import { useParams } from 'react-router-dom';
import HttpMiddleware from "../../HttpMiddleware";
import Message from '../message';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';

const Aluno = () => {

  const [idcad, setIdcad] = useState(0);
  const [nome, setNome] = useState('');
  const [login, setLogin] = useState('');
  const [tipo, setTipo] = useState('');
  const [ativo, setAtivo] = useState('true');

  const [password, setPassword] = useState('');
  const [passwordconfirmation, setPasswordconfirmation] = useState('');

  const [btnalterarsenha, setBtnalterarsenha] = useState(false)
  const [exibesenha, setExibesenha] = useState(false)

  const [professores, setProfessores] = useState([]);
  const [professorid, setProfessorid] = useState(0);
  const [professordata, setProfessordata] = useState([]);

  const [alunos, setAlunos] = useState([]);
  const [alunoid, setAlunoid] = useState(0);
  const [alunodata, setAlunodata] = useState([]);

  const [loading, setLoading] = useState(true);
  const [saved, setSaved] = useState(false);
  const [saveMsg, setSaveMsg] = useState('');
  const [saveType, setSaveType] = useState('success');

  let { id } = useParams();
  //const [posts, setPosts] = useState({ post: null, countSecrets: 0, ui: '' });

  useEffect(() => {
    /*if ( id !== 0 ) {
      var url = 'admin/escolas/' + id
      var res = new HttpMiddleware().execPromisse( url, 'GET', true, null );
      res.then((response) => {
        console.log(response.data);
        setData(response.data)
      })
    }*/

    if ( id !== '0' ) {

      setIdcad(parseInt(id));


      const fetchData = async () =>{

        try {
          var url = 'admin/users/' + id
          var res = await new HttpMiddleware().exec( url, 'GET', true, null );
          console.log(res);
          setNome(res.nome)
          setLogin(res.login)
          setTipo(res.tipo || 'ADMIN')
          setAtivo(res.ativo ? 'true' : 'false')
          if ( res.tipo === 'PROFESSOR' ) {
            loadingProfessores();
            setProfessorid(res.professor_id)
            setProfessordata(res.professor)
          }

          if ( res.tipo === 'ALUNO' ) {
            loadingAlunos();
            setAlunoid(res.aluno_id)
            setAlunodata(res.aluno)
          }

          console.log('carr');
        } catch (error) {
          console.error(error.message);
        }
      }
      if ( loading ) {
        fetchData();
        setLoading(false);
      }
    } else {
      setExibesenha(true)
    }
  }, [id, loading]);

  const save = () => {
    setSaved(false)
    setLoading(true)
    var dadosEnviar = {
      'nome': nome,
      'email': login,
      'tipo': tipo,
      'ativo': ativo === 'true' ? true : false,
      'professor_id': tipo === 'PROFESSOR' ? professorid : null,
      'aluno_id': tipo === 'ALUNO' ? alunoid : null,
      'password': password
    }

    if ( idcad === 0 ) {
      if ( password === '' ) {
        setSaveMsg('A senha precisa ser preenchida')
        setSaveType('fail')
        setLoading(false)
        setSaved(true)
        return;
      }

      if ( password !== passwordconfirmation  ) {
        setSaveMsg('A senha e a confirmação precisam ser idênticas')
        setSaveType('fail')
        setLoading(false)
        setSaved(true)
        return;
      }
    }

    if ( dadosEnviar.tipo === 'PROFESSOR' && ( !dadosEnviar.professor_id || dadosEnviar.professor_id === 0 ) ) {
        setSaveMsg('O professor precisa ser preenchido para o usuário tipo Professor')
        setSaveType('fail')
        setLoading(false)
        setSaved(true)
        return;
    }
    if ( dadosEnviar.tipo === 'ALUNO' && ( !dadosEnviar.aluno_id || dadosEnviar.aluno_id === 0 ) ) {
        setSaveMsg('O aluno precisa ser preenchido para o usuário tipo Aluno')
        setSaveType('fail')
        setLoading(false)
        setSaved(true)
        return;
    }

    console.log(dadosEnviar);

    const sendData = async () =>{
      try {
        var method = 'POST';
        var url = 'admin/users';
        setSaveMsg('Dados salvos com sucesso!')
        setSaveType('success')
        if ( idcad !== 0 ) {
            method = 'PUT';
            url += '/' + idcad;
            setSaveMsg('Dados alterados com sucesso!')
        }

        var res = await new HttpMiddleware().exec( url, method, true, dadosEnviar );
        console.log(res);
        if ( res.errors ) {
          setSaveMsg(res.errors[0].message)
          setSaveType('fail')
        } else {
          setIdcad( res.id );
          setExibesenha(false)
        }
        setLoading(false)
        setSaved(true)
      } catch (error) {
        console.error(error.message);
        setLoading(false)
        setSaved(true)
        setSaveMsg('Erro ao salvar dados!')
        setSaveType('fail')
      }
    }

    sendData();
  }


  const getTipo = (event) => {

    var tip = event.target.value

    setTipo(tip)

    if ( tip === 'PROFESSOR' ) {
      loadingProfessores();
    }

    if ( tip === 'ALUNO' ) {
      loadingAlunos();
    }

    console.log(tip);

  }

  const getProfessor = (event,val) => {
      professores.forEach(i => {
        if ( i.nome === val ) {
          setProfessorid(i.id)
          setProfessordata(i)
          setLogin(i.email)
          setNome(i.nome)
        }
      });

      console.log(professorid);

  }

  const getAluno = (event,val) => {
      alunos.forEach(i => {
        if ( i.nome === val ) {
          setAlunoid(i.id)
          setAlunodata(i)
          setLogin(i.email)
          setNome(i.nome)
        }
      });

      console.log(alunoid);

  }

  const loadingProfessores = () => {
    const lProf = async () =>{
      try {
        var urlProf = 'admin/professores?no_paginate=true';
        var resProf = await new HttpMiddleware().exec( urlProf, 'GET', true, null );

        setProfessores(resProf)

      } catch (error) {
        console.error(error.message);
      }
    }
    lProf();
  }

  const loadingAlunos = () => {
    const lAl = async () =>{
      try {
        var urlAl = 'admin/alunos?no_paginate=true';
        var resAl = await new HttpMiddleware().exec( urlAl, 'GET', true, null );

        setAlunos(resAl)

      } catch (error) {
        console.error(error.message);
      }
    }
    lAl();
  }

  const alterarSenha = () => {
    setExibesenha(true)
    setBtnalterarsenha(true)
  }

  const getPassword = (event) => {
    setPassword(event.target.value)
    console.log(password);
  }

  const getPasswordconfimarion = (event) => {
    setPasswordconfirmation(event.target.value)
  }

  const execalterarsenha = () => {

    setSaved(false)
    setLoading(true)

    if ( password === '' ) {
      setSaveMsg('A senha precisa ser preenchida')
      setSaveType('fail')
      setLoading(false)
      setSaved(true)
      return;
    }

    if ( password !== passwordconfirmation  ) {
      setSaveMsg('A senha e a confirmação precisam ser idênticas')
      setSaveType('fail')
      setLoading(false)
      setSaved(true)
      return;
    }

    var dadosEnviar = {
      'password': password,
      'password_confirmation': passwordconfirmation
    }

    const sendData = async () =>{
      try {
        var method = 'PUT';
        var url = 'admin/userspass/' + idcad;
        setSaveMsg('Senha alterada com sucesso!')
        setSaveType('success')

        var res = await new HttpMiddleware().exec( url, method, true, dadosEnviar );
        console.log(res);
        if ( res.errors ) {
          setSaveMsg(res.errors[0].message)
          setSaveType('fail')
        }
        setLoading(false)
        setSaved(true)
        setExibesenha(false)
        setBtnalterarsenha(false)
      } catch (error) {
        console.error(error.message);
        setLoading(false)
        setSaved(true)
        setExibesenha(false)
        setBtnalterarsenha(false)
        setSaveMsg('Erro ao salvar dados!')
        setSaveType('fail')
      }
    }

    sendData();
  }


  return (
    <div>
      { loading &&
        <div id="loading">
          <span>Carregando...</span>
        </div>
      }
      { saved &&
        <Message duration="5000" message={saveMsg} type={saveType} />
      }

      <h4>Cadastro de Usuário</h4>
      <div className="button-group">
        <button style={style.btnnovo} onClick={() => save()}>Salvar</button>
        { idcad !== 0 &&
          <button style={style.btnalterar} onClick={() => alterarSenha()}>Alterar Senha</button>
        }
      </div>
      <hr/>
      <form className='row'>
        <label className="col-sm-4" style={style.label}>Nome:
          <input style={style.input} type="text" value={nome || ''} onChange={e=> setNome(e.target.value)}/>
        </label>
        <label className="col-sm-4" style={style.label}>E-Mail:
          <input style={style.input} type="text" value={login || ''} onChange={e=> setLogin(e.target.value)}/>
        </label>
        <label className="col-sm-2" style={style.label}>Tipo:
          <select style={style.input} value={tipo || ''}onChange={getTipo}>
            <option value="ADMIN">Admin</option>
            <option value="PROFESSOR">Professor</option>
            <option value="ALUNO">Aluno</option>
          </select>
        </label>
        <label className="col-sm-2" style={style.label}>Ativo:
          <select style={style.input} value={ativo || 'true'}onChange={e=> setAtivo(e.target.value)}>
            <option value="true">Sim</option>
            <option value="false">Não</option>
          </select>
        </label>
        { tipo === 'PROFESSOR' &&
          <div className="col-sm-12">
            <label style={style.label}>Professor:</label>
            <Autocomplete
                value={professorid}
                options={professores}
                renderOption={prof => <Fragment>{prof.nome}</Fragment>}
                getOptionLabel={prof => typeof prof.nome === 'string'
                    || prof.nome instanceof String ? prof.nome : ""}
                style={style.inputselect}
                renderInput={params => {
                    return (
                        <TextField
                            {...params}
                            label={professordata.nome}
                            variant="outlined"
                            fullWidth
                        />
                    )
                }}
                onInputChange={getProfessor}
            />
          </div>
        }
        { tipo === 'ALUNO' &&
          <div className="col-sm-12">
            <label style={style.label}>Aluno:</label>
            <Autocomplete
                value={alunoid}
                options={alunos}
                renderOption={al => <Fragment>{al.nome}</Fragment>}
                getOptionLabel={al => typeof al.nome === 'string'
                    || al.nome instanceof String ? al.nome : ""}
                style={style.inputselect}
                renderInput={params => {
                    return (
                        <TextField
                            {...params}
                            label={alunodata.nome}
                            variant="outlined"
                            fullWidth
                        />
                    )
                }}
                onInputChange={getAluno}
            />
          </div>
        }
        { exibesenha &&
          <div className="separator"></div>
        }
        { exibesenha &&
          <label className="col-sm-6" style={style.label}>Senha:
            <input style={style.input} type="password" value={password || ''} onChange={getPassword}/>
          </label>
        }
        { exibesenha &&
          <label className="col-sm-6" style={style.label}>Confirmação de senha:
            <input style={style.input} type="password" value={passwordconfirmation || ''} onChange={getPasswordconfimarion}/>
          </label>
        }
      </form>
      { btnalterarsenha &&
        <button className="col-sm-12" style={style.btnalterarsenha} onClick={() => execalterarsenha()}>Alterar Senha</button>
      }
    </div>
  );
}

const style = {
  label: {
    display: 'block',
    fontSize: 12
  },
  input: {
    display: 'block',
    fontSize: 15,
    padding: 3,
    width: '100%'
  },
  inputselect: {
    display: 'block',
    fontSize: 15,
    width: '100%',
    padding: '0px',
    margin: '0px'
  },
  submit: {
    border: '1px solid',
    position: 'absolute',
    right: '4px',
    marginTop: '-40px',
    padding: '3px 10px',
    borderRadius: '0 3px 3px 0'
  },
  editbutton: {
    border: 'none',
    background: 'none'
  },
  btnnovo: {
    padding: '2px 6px 6px',
    border: '1px solid',
    borderRadius: '3px',
    background: '#007bff',
    color: '#fff',
    margin: '3px -1px'
  },
  btnalterar: {
    padding: '2px 6px 6px',
    border: '1px solid',
    borderRadius: '3px',
    background: 'rgb(4 175 33)',
    color: '#fff',
    margin: '3px -1px'
  },
  btnalterarsenha: {
    padding: '2px 6px 6px',
    border: '1px solid',
    borderRadius: '3px',
    background: 'rgb(203 129 17)',
    color: '#fff',
    margin: '16px -1px',
    height: '35px'
  },
  btnnovoescola: {
    padding: '2px 6px 5px',
    border: '1px solid',
    margin: '5px',
    borderRadius: '3px',
    background: '#007bff',
    color: '#fff',
    fontSize: 15
  },
  btndeleteschool: {
    padding: '2px 6px 5px',
    border: '1px solid',
    margin: '5px',
    borderRadius: '3px',
    background: 'rgb(229 68 68)',
    color: '#fff',
    fontSize: 15
  },
  btnaddschool: {
    padding: '2px 6px 5px',
    border: '1px solid',
    margin: '5px',
    borderRadius: '3px',
    background: 'rgb(8 161 62)',
    color: '#fff',
    fontSize: 15
  },
  schoollist: {
    border: '1px solid #009533',
    marginBottom: '3px',
    borderRadius: '3px',
    background: 'rgb(160 255 193)'
  },
  textescola: {
    fontSize: 20,
    fontWeight: 'bold'
  },
  modal: {
    position: 'fixed',
    right: 0,
    left: 0,
    top: 0,
    bottom: 0,
    background: 'rgba(0,0,0,.5)',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  btnclosemodal: {
    padding: '2px 6px 5px',
    border: '1px solid',
    margin: '5px',
    borderRadius: '3px',
    background: 'rgb(229 68 68)',
    color: '#fff',
    fontSize: 15,
    position: 'absolute',
    right: '5%',
    top: '5%'
  },
  internalmodal: {
    background: '#fff',
    width: '90%',
    height: '90%',
    padding: '10px'
  }
};

export default Aluno;
