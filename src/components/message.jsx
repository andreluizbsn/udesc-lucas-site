import React, { Component } from "react";
import FlashMessage from 'react-flash-message'

class Message extends Component {
  constructor(props) {
    super(props);
    this.state = {duration: props.duration, message: props.message, type: props.type};
  }

  render() {
    return (
      <div>
        <div className={this.state.type}>
          <FlashMessage duration={this.state.duration} persistOnHover={true}>
            <strong>{this.state.message}</strong>
          </FlashMessage>
        </div>
      </div>
    );
  }
}

const style = {
  label: {
    display: 'block',
    fontSize: 12
  },
  input: {
    display: 'block',
    fontSize: 15,
    padding: 3,
    width: '100%'
  },
  submit: {
    border: '1px solid',
    position: 'absolute',
    right: '4px',
    marginTop: '-40px',
    padding: '3px 10px',
    borderRadius: '0 3px 3px 0'
  },
  editbutton: {
    border: 'none',
    background: 'none'
  },
  btnnovo: {
    padding: '2px 6px 6px',
    border: '1px solid',
    margin: '5px',
    borderRadius: '3px',
    background: '#007bff',
    color: '#fff',
    position: 'absolute',
    right: 0,
    top: 0,
  }
};

export default Message;
