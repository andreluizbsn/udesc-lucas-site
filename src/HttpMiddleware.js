
class HttpMiddleware {

  async exec( url, method, auth, data, token ) {

    var headers = {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    }

    if ( auth ) {
      var user = JSON.parse( sessionStorage.getItem('user') )
      headers.Authorization = user.token;
    } else if ( token ) {
      headers.Authorization = token;
    }

    var opt = {
      method: method,
      headers: headers
    }

    if ( method === 'POST' || method === 'PUT' ) {
      opt.body = JSON.stringify(data)
    }

    try {
      const response = await fetch("http://localhost:8888/v1/" + url, opt);
      const json = await response.json();
      console.log(url);
      console.log( json );
      return json;
    } catch (error) {
      console.error(error);
      return error;
    }
  }

  async execUpload( local, id, image_field, image ) {

    const formData = new FormData();

    formData.append('image', image);

    var user = JSON.parse( sessionStorage.getItem('user') )

    var headers = {
      'Content-Type': 'multipart/form-data',
      'Authorization': user.token
    }

    var opt = {
      method: 'POST',
      headers: headers,
      body: formData
    }

    try {
      const response = await fetch("http://localhost:8888/v1/admin/image/" + local + "/" + id + "/" + image_field, opt);
      const json = await response.json();
      console.log( json );
      return json;
    } catch (error) {
      console.error(error);
      return error;
    }
  }

  async execUpload2( local, id, image_field, image ) {

    var url = "http://localhost:8888/v1/admin/image/" + local + "/" + id + "/" + image_field;

     var formData: any = new FormData();
     formData.append("image", image);

     var xhr = new XMLHttpRequest;
     xhr.addEventListener("load", function (evt) {
       return evt;
     }, false);
     xhr.addEventListener("error", function (evt) {
       return evt;
     }, false);
     xhr.open('POST', url, true);
     //xhr.setRequestHeader('Content-Type', 'multipart/form-data')
     xhr.setRequestHeader('Authorization', JSON.parse( sessionStorage.getItem('user') ).token)
     xhr.send(formData);

   }

}

export default HttpMiddleware;
